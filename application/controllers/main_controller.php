<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

/**
* Description of Product_model
*
* @author https://roytuts.com
*/

class Main_controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
    }
    public function index()
    {
        $data = array();
        // Get messages from the session 
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        $data['viewdata'] = $this->main_model->getRows();
        $data['title'] = 'Listing Page';
        $this->load->view('main_view', $data);
    }
    public function re_direct() {
		$data['salesinfo'] = $this->main_model->getRows();
        $this->load->view('pdf_view.php',$data);
    }
    // Generate pdf 
    public function generate_pdf() {
		//load pdf library
		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('https://roytuts.com');
		$pdf->SetTitle('General details of Person');
		$pdf->SetSubject('Report generated using Codeigniter and TCPDF');
		$pdf->SetKeywords('TCPDF, PDF, MySQL, Codeigniter');

		// set default header data
		//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set font
		$pdf->SetFont('times', 'BI', 12);
		
		// ---------------------------------------------------------
		
		
		//Generate HTML table data from MySQL - start
		// $template = array(
		// 	'table_open' => '<table border="1" cellpadding="2" cellspacing="1">'
		// );

		// $this->table->set_template($template);

		$this->table->set_heading('Name', 'Mobile', 'Email', 'Gender', 'Country Name','State Name','City Name','Address','File Name');
		
		$salesinfo = $this->main_model->getRows();
			
        // echo "<pre>"; print_r($salesinfo);die;
		foreach ($salesinfo as $key => $sf):
			$this->table->add_row($sf->name, $sf->mobile, $sf->email, $sf->gender, $sf->country_name, $sf->state_name, $sf->city_name, $sf->address, $sf->file_name);
		endforeach;
		
		$html = $this->table->generate();
		//Generate HTML table data from MySQL - end
		
		// add a page
		$pdf->AddPage();
		
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');
		
		// reset pointer to the last page
		$pdf->lastPage();

		//Close and output PDF document
		$pdf->Output(md5(time()) . '.pdf', 'D');
	}

    // Export pdf file 
    // function export_pdf()
    // {
    //     $usersData = $this->main_model->getRows();
    //     $output = "<h3 align='center'>Convert HTML to PDF in CodeIgniter using Dompdf</h3>";
    //     $output .= '<table style="margin-left: 15px;margin: right 5px;">';
    //     $output .= '<thead>';
    //     $output .= '<tr>';
    //     $output .= '<th width="2%">#</th>';
    //     $output .= '<th width="10%">Profile Pic</th>';
    //     $output .= '<th width="10%">Name</th>';
    //     $output .= '<th width="7%">Mobile no.</th>';
    //     $output .= '<th width="15%">Email</th>';
    //     $output .= '<th width="9%">Gender</th>';
    //     $output .= '<th width="10%">Country</th>';
    //     $output .= '<th width="10%">State</th>';
    //     $output .= '<th width="9%">City</th>';
    //     $output .= '<th width="13%">Address</th>';
    //     $output .= '</tr>';
    //     $output .= '</thead>';
    //     $output .= '<tbody>';
    //     $count = 0;
    //     foreach ($usersData as $key => $rowdata) {
    //         $output .= '<tr>
    //                     <td>' . (++$count) . '</td>
    //                     <td>' . $rowdata['file_name'] . '</td>
    //                     <td>' . $rowdata['name'] . '</td>
    //                     <td>' . $rowdata['mobile'] . '</td>
    //                     <td>' . $rowdata['email'] . '</td>
    //                     <td>' . $rowdata['gender'] . '</td>
    //                     <td>' . $rowdata['country_name'] . '</td>
    //                     <td>' . $rowdata['state_name'] . '</td>
    //                     <td>' . $rowdata['city_name'] . '</td>
    //                     <td>' . $rowdata['address'] . '</td>        
    //                     </tr>';
    //     }
    //     $output .= '</tbody>';
    //     $output .= '</table>';
    //     // echo "<pre>";
    //     // print_r($output);
    //     // die;
    //     $this->pdf->loadHtml($output);
    //     $this->pdf->render();

    //     $name = rand(100, 1000);

    //     // $this->pdf->stream("' . $name . '.pdf", array("Attachment" => 0));
    //     $this->pdf->stream("' . $name . '.pdf", array("Attachment" => false));

    //     // $this->pdf->stream();

    //     exit(0);

    // }
    // Export excel file
    function export_csv()
    {
        // File name
        $number = rand(100, 1000);
        $filename = 'users_' . $number . '.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
        // Get data
        $usersData = $this->main_model->getRows();
        // File creation
        $file = fopen('php://output', 'w');
        $header = array("Name", "Mobile", "E-mail", "Gender", "Country Name", "State Name", "City Name", "Address", "Profile pic", "Id");
        fputcsv($file, $header);
        foreach ($usersData as $key => $line) {
            fputcsv($file, $line);
        }
        fclose($file);
        exit;
    }
    // Import excel file
    function import_csv()
    {
        $postData = array();

        if (isset($_POST["submit"])) {
            $file = $_FILES['file']['tmp_name'];
            $handle = fopen($file, "r");
            $c = 0;
            while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
                $postData['name'] = $filesop[0];
                $postData['mobile'] = $filesop[1];
                $postData['email'] = $filesop[2];
                $postData['gender'] = $filesop[3];
                $postData['country'] = $this->main_model->get_country_id($filesop[4]);
                $postData['state'] = $this->main_model->get_state_id($filesop[5]);
                $postData['city'] = $this->main_model->get_city_id($filesop[6]);
                $postData['address'] = $filesop[7];
                $postData['file_name'] = $filesop[8];

                if ($c <> 0) {                    /* SKIP THE FIRST ROW */
                    $this->main_model->insertData($postData);
                }
                $c = $c + 1;
            }
            $this->session->set_userdata('success_msg', 'Gallery has been added successfully.');
            redirect('main_controller');
        }
    }
    // Add function
    public function add()
    {
        $data = array();
        $errorUpload = '';

        if ($this->input->post('postSubmit')) {
            $this->form_validation->set_rules('name', 'name', 'required');
            $this->form_validation->set_rules('mobile', 'mobile', 'required|is_natural|is_unique[maintable.mobile]');
            $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[maintable.email]');
            $this->form_validation->set_rules('gender', 'gender', 'required');
            $this->form_validation->set_rules('country', 'country', 'required');
            $this->form_validation->set_rules('state', 'state', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('address', 'address', 'required');

            if ($this->form_validation->run() == true) {
                $postData = array(
                    'name' => $this->input->post('name'),
                    'mobile' => $this->input->post('mobile'),
                    'email' => $this->input->post('email'),
                    'gender' => $this->input->post('gender'),
                    'country' => $this->input->post('country'),
                    'state' => $this->input->post('state'),
                    'city' => $this->input->post('city'),
                    'address' => $this->input->post('address')
                );

                if (!empty($_FILES['file_name']['name'])) {
                    $_FILES['file']['name']     = $_FILES['file_name']['name'];
                    $_FILES['file']['type']     = $_FILES['file_name']['type'];
                    $_FILES['file']['tmp_name'] = $_FILES['file_name']['tmp_name'];
                    $_FILES['file']['error']    = $_FILES['file_name']['error'];
                    $_FILES['file']['size']     = $_FILES['file_name']['size'];

                    // File upload configuration 
                    $uploadPath = 'imagesfolder/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';

                    // Load and initialize upload library 
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    // Upload file to server 
                    if ($this->upload->do_upload('file')) {
                        // Uploaded file data 
                        $fileData = $this->upload->data();
                        $postData['file_name'] = $fileData['file_name'];
                    }
                }
                $uploadData = $postData;

                if (!empty($uploadData)) {
                    // Insert files info into the database 
                    $insert = $this->main_model->insertImage($uploadData);
                }
                $this->session->set_userdata('success_msg', 'Gallery has been added successfully.');
                redirect('main_controller');
            }
        }
        // Insert country
        $data['country'] = $this->main_model->fetch_country();
        $data['title'] = 'Insert Data Page';
        $data['heading'] = 'ADD';
        $this->load->view('insert_edit_view', $data);
    }
    // File validation function
    public function file_check($str)
    {
        $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
        return false;
    }
    // Edit function
    public function edit($id)
    {
        $data = $getdata = array();
        $getdata = $this->main_model->getRows($id);

        if ($this->input->post('postSubmit')) {
            $this->form_validation->set_rules('name', 'name', 'required');
            $this->form_validation->set_rules('mobile', 'mobile', 'required|is_natural');
            $this->form_validation->set_rules('email', 'email', 'required|valid_email');
            $this->form_validation->set_rules('gender', 'gender', 'required');
            $this->form_validation->set_rules('country', 'country', 'required');
            $this->form_validation->set_rules('state', 'state', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('address', 'address', 'required');

            if ($this->form_validation->run() == true) {
                $postData = array(
                    'name' => $this->input->post('name'),
                    'mobile' => $this->input->post('mobile'),
                    'email' => $this->input->post('email'),
                    'gender' => $this->input->post('gender'),
                    'country' => $this->input->post('country'),
                    'state' => $this->input->post('state'),
                    'city' => $this->input->post('city'),
                    'address' => $this->input->post('address')
                );

                if (!empty($_FILES['file_name']['name'])) {
                    $_FILES['file']['name']     = $_FILES['file_name']['name'];
                    $_FILES['file']['type']     = $_FILES['file_name']['type'];
                    $_FILES['file']['tmp_name'] = $_FILES['file_name']['tmp_name'];
                    $_FILES['file']['error']    = $_FILES['file_name']['error'];
                    $_FILES['file']['size']     = $_FILES['file_name']['size'];

                    // File upload configuration 
                    $uploadPath = 'imagesfolder/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';

                    // Load and initialize upload library 
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    // Upload file to server 
                    if ($this->upload->do_upload('file')) {
                        // Uploaded file data 
                        $fileData = $this->upload->data();
                        $postData['file_name'] = $fileData['file_name'];
                        unlink("imagesfolder/" . ($_POST['file_name']));
                    }
                }
                $uploadData = $postData;

                if (!empty($uploadData)) {
                    // Insert files info into the database 
                    $update = $this->main_model->update($uploadData, $id);
                }
                $this->session->set_userdata('success_msg', 'Data has been updated successfully.');
                redirect('main_controller');
            }
        }

        $data['viewdata'] = $getdata;
        $data['country'] = $this->main_model->fetch_country();
        $data['title'] = 'Edit Data Page';
        $data['heading'] = 'EDIT';
        $this->load->view('insert_edit_view', $data);
    }
    // Delete function
    public function delete($id)
    {
        if ($id) {
            $getdata = $this->main_model->getRows($id);
            $delete = $this->main_model->delete($id);
            unlink("imagesfolder/" . ($getdata['file_name']));

            if ($delete) {
                $this->session->set_userdata('success_msg', 'Data has been removed successfully.');
            } else {
                $this->session->set_userdata('error_msg', 'Some problems occurred while deleting, please try again.');
            }
        }
        redirect('main_controller');
    }
    // View single row data
    public function view($id)
    {
        $data = array();

        if (!empty($id)) {
            $data['viewdata'] = $this->main_model->getRows($id);
            // Load single row veiw data page
            $this->load->view('data_view', $data);
        }
    }
    // Fetch State
    function fetch_state()
    {
        if ($this->input->post('country_id')) {
            echo $this->main_model->fetch_state($this->input->post('country_id'));
        }
    }
    // Fetch City
    function fetch_city()
    {
        if ($this->input->post('state_id')) {
            echo $this->main_model->fetch_city($this->input->post('state_id'));
        }
    }
}
