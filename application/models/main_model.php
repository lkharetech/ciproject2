<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

/**
* Description of Product_model
*
* @author https://roytuts.com
*/

class Main_model extends CI_Model {
    // View data
    function getRows($id = "") {
        if (!empty($id)) {
            $this->db->select ('countries.country_name,countries.country_id,states.state_name,states.state_id,cities.city_name,cities.city_id,maintable.name,maintable.mobile,maintable.email,maintable.gender,maintable.address,maintable.file_name,maintable.id'); 
            $this->db->from ('maintable');
            $this->db->join ('countries', 'countries.country_id = maintable.country' , 'left' );
            $this->db->join ('states', 'states.state_id = maintable.state' , 'left' );
            $this->db->join ('cities', 'cities.city_id = maintable.city' , 'left' );
            $this->db->where ( 'maintable.id', $id);
            $query = $this->db->get();
            return $query->row_array();
        } else {
            $this->db->select ('maintable.name,maintable.mobile,maintable.email,maintable.gender,countries.country_name,states.state_name,cities.city_name,maintable.address,maintable.file_name,maintable.id'); 
            $this->db->from ('maintable');
            $this->db->join ('countries', 'countries.country_id = maintable.country' , 'left' );
            $this->db->join ('states', 'states.state_id = maintable.state' , 'left' );
            $this->db->join ('cities', 'cities.city_id = maintable.city' , 'left' );
            $query = $this->db->get();
            // return $query->result_array();
            return $query->result();
        }
    }
    // Get country id after inserting country name
    function get_country_id($country_name) {
        if(!empty($country_name)) {
            $this->db->select('countries.country_id');
            $this->db->where('countries.country_name', $country_name);
            $query = $this->db->get('countries');
            $output = "";
            foreach($query->row() as $row) {
                $output = $row;
            }
            return $output;
        } else {
            echo "Country Name is invalid";
        }
    }
    // Get state id after inserting state name
    function get_state_id($state_name) {
        if(!empty($state_name)) {
            $this->db->select('states.state_id');
            $this->db->where('states.state_name', $state_name);
            $query = $this->db->get('states');
            $output = "";
            foreach($query->row() as $row) {
                $output = $row;
            }
            return $output;
        } else {
            echo "State Name is invalid";
        }
    }
    // Get state id after inserting state name
    function get_city_id($city_name) {
        if(!empty($city_name)) {
            $this->db->select('cities.city_id');
            $this->db->where('cities.city_name', $city_name);
            $query = $this->db->get('cities');
            $output = "";
            foreach($query->row() as $row) {
                $output = $row;
            }
            return $output;
        } else {
            echo "City Name is invalid";
        }
    }
    // Insert data into the database 
    public function insertImage($data = array()) { 
        if(!empty($data)){ 
            // Insert gallery data 
            $insert = $this->db->insert('maintable', $data); 
            // Return the status 
            return $insert?$this->db->insert_id():false; 
        } 
        return false; 
    } 
    // Insert data into database from excel file 
    public function insertData($data = array()) { 
        if(!empty($data)){ 
            // Insert gallery data 
            $insert = $this->db->insert('maintable', $data); 
            // Return the status 
            return $insert?$this->db->insert_id():false; 
        } 
        return false; 
    } 
    // Update data
    function update($data, $id) {
        if(!empty($data) && !empty($id)) {
            $update = $this->db->update('maintable', $data, array('id' => $id));
            return $update?true:false;
        }
        return false;
    }
    // Delete data
    function delete($id) {
        $delete = $this->db->delete('maintable', array('id' => $id)); 
        return $delete?true:false; 
    }
    // View Country
    function fetch_country() {
        $this->db->order_by("country_name", "ASC");
        $query = $this->db->get("countries");
        return $query->result_array();
    }
    // View state
    function fetch_state($country_id) {
        $this->db->where('country_id', $country_id);
        $this->db->order_by('state_name', 'ASC');
        $query = $this->db->get('states');
        $output = '<option value="">Select State</option>';
        foreach ($query->result() as $row) {
            $output .= '<option value="' . $row->state_id . '">' . $row->state_name . '</option>';
        }
        return $output;
    }
    // View city
    function fetch_city($state_id) {
        $this->db->where('state_id', $state_id);
        $this->db->order_by('city_name', 'ASC');
        $query = $this->db->get('cities');
        $output = '<option value="">Select City</option>';
        foreach ($query->result() as $row) {
            $output .= '<option value="' . $row->city_id . '">' . $row->city_name . '</option>';
        }
        return $output;
    }
}
