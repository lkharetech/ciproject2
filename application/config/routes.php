<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['main_controller'] = 'main_controller';
$route['main_controller/add'] = 'main_controller/add';
$route['main_controller/view/(:num)'] = 'main_controller/view/$1';
$route['main_controller/edit/(:num)'] = 'main_controller/edit/$1';
$route['main_controller/delete/(:num)'] = 'main_controller/delete/$1';
$route['main_controller/fetch_state'] = 'main_controller/fetch_state';
$route['main_controller/fetch_city'] = 'main_controller/fetch_city';
$route['export_csv'] = 'main_controller/export_csv';
$route['import_csv'] = 'main_controller/import_csv';
$route['generate_pdf'] = 'main_controller/generate_pdf';
$route['re_direct'] = 'main_controller/re_direct';






