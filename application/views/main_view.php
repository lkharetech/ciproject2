<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <title><?php echo $title; ?></title>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
        }
    </style>
</head>

<body>
    <div>
        <h2 style="text-align: center;">CI CRUD & DYNAMIC DEPENDENCY 2</h2>
        <!-- Display status message -->
        <?php if (!empty($success_msg)) { ?>
            <div class="col-xs-12">
                <div class="alert alert-success"><?php echo $success_msg; ?></div>
            </div>
        <?php } elseif (!empty($error_msg)) { ?>
            <div class="col-xs-12">
                <div class="alert alert-danger"><?php echo $error_msg; ?></div>
            </div>
        <?php } ?>

        <div>
            <button style="margin-left: 15px;" onclick="window.location.href='<?php echo site_url('main_controller/add'); ?>'">INSERT DATA</button>
            <button style="margin-left: 15px;" onclick="window.location.href='<?php echo site_url('export_csv'); ?>'">EXPORT EXCEL</button>
            <button style="margin-left: 15px;" onclick="window.location.href='<?php echo site_url('generate_pdf'); ?>'">EXPORT PDF</button><br><br>
            <div>
                <form style="margin-left: 15px;" action="<?php echo site_url('import_csv'); ?>" enctype="multipart/form-data" method="post" role="form">
                    <div class="form-group">
                        <label for="exampleInputFile">File Upload</label>
                        <input type="file" name="file" id="file">
                        <p class="help-block">Only Excel/CSV File Import.</p>
                    </div>
                    <button type="submit" name="submit" value="submit">Upload</button>
                </form>
            </div>
            <h4 style="text-align:center;">LISTING OF FIELDS</h4>
            <hr>
        </div>
        <div>
            <table style="margin-left: 15px;margin: right 5px;">
                <thead>
                    <tr>
                        <th width="2%">#</th>
                        <th width="10%">Profile Pic</th>
                        <th width="10%">Name</th>
                        <th width="7%">Mobile no.</th>
                        <th width="15%">Email</th>
                        <th width="9%">Gender</th>
                        <th width="10%">Country</th>
                        <th width="10%">State</th>
                        <th width="9%">City</th>
                        <th width="13%">Address</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody id="userData">
                <tbody>
                    <?php $count = 0;
                    if (!empty($viewdata)) : foreach ($viewdata as $rowdata) :
                            $defaultImage = !empty($rowdata['file_name']) ? '<img src="' . base_url() . 'imagesfolder/' . $rowdata['file_name'] . '" width="100px" height="100px"/>' : '';
                    ?>
                            <tr>
                                <td><?php echo '#' . ++$count; ?></td>
                                <td><?php echo $defaultImage; ?></td>
                                <td><?php echo $rowdata['name']; ?></td>
                                <td><?php echo $rowdata['mobile']; ?></td>
                                <td><?php echo $rowdata['email']; ?></td>
                                <td><?php echo $rowdata['gender']; ?></td>
                                <td><?php echo $rowdata['country_name']; ?></td>
                                <td><?php echo $rowdata['state_name']; ?></td>
                                <td><?php echo $rowdata['city_name']; ?></td>
                                <td><?php echo $rowdata['address']; ?></td>
                                <td>
                                    <a href="<?php echo site_url('main_controller/view/' . $rowdata['id']); ?>" class="glyphicon glyphicon-eye-open"></a>&nbsp;&nbsp;
                                    <a href="<?php echo site_url('main_controller/edit/' . $rowdata['id']); ?>" class="glyphicon glyphicon-edit"></a>&nbsp;&nbsp;
                                    <a href="<?php echo site_url('main_controller/delete/' . $rowdata['id']); ?>" class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure to delete?')"></a>
                                </td>
                            </tr>
                        <?php endforeach;
                    else : ?>
                        <tr>
                            <td colspan="4">Data not found...... (No data inserted)</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>