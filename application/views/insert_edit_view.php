<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>
    <!-- Bootstrap CSS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <div>
            <h3 style="text-align: center;"><?php echo $heading; ?> DATA</h3>
            <button onclick="window.location.href='<?php echo site_url('main_controller'); ?>'">HOME</button>
            <hr>
        </div>
        <div>
            <form method="post" class="form" enctype="multipart/form-data">
                <div>
                    <label for="name">Name :</label>
                    <input type="text" id="name" name="name" value="<?php echo isset($viewdata['name'])?$viewdata['name']:''; ?>" placeholder="Enter your name">
                    <span class="text-danger"><?php echo form_error('name'); ?></span>
                </div>
                <div><br>
                    <label for="mobile">Mobile No. :</label>
                    <input type="text" id="mobile" name="mobile" value="<?php echo isset($viewdata['mobile'])?$viewdata['mobile']:''; ?>" placeholder="Enter your mobile no.">
                    <span class="text-danger"><?php echo form_error('mobile'); ?></span>
                </div>
                <div><br>
                    <label for="email">Email id :</label>
                    <input type="text" id="email" name="email" value="<?php echo isset($viewdata['email'])?$viewdata['email']:''; ?>" placeholder="Enter your email-id">
                    <span class="text-danger"><?php echo form_error('email'); ?></span>
                </div>
                <div><br>
                    <label>Choose your gender :</label>
                    <input type="radio" id="male" name="gender" value="male" <?php echo set_value('gender', isset($viewdata['gender'])?$viewdata['gender']:'') == 'male' ? "checked" : ""; ?> >
                    <label for="male">Male</label>
                    <input type="radio" id="female" name="gender" value="female" <?php echo set_value('gender', isset($viewdata['gender'])?$viewdata['gender']:'') == 'female' ? "checked" : ""; ?>>
                    <label for="female">Female</label><br>
                    <span class="text-danger"><?php echo form_error('gender'); ?></span>
                </div>
                <div><br>
                    <label for="country">Choose your country :</label>
                    <select name="country" id="country">
                        <option value="">Select Country</option>
                        <?php 
                         if (isset($viewdata) && $viewdata != '') { ?>
                            <option value="<?php echo $viewdata['country_id']; ?>"<?php echo 'selected="selected"'?>><?php echo ($viewdata['country_name']); ?></option>
                        <?php } 
                        foreach ($country as $row) {
                            echo '<option value="' . $row['country_id'] . '">' . $row['country_name'] . '</option>';
                        }
                        ?>
                    </select>
                    <span class="text-danger"><?php echo form_error('country'); ?></span>
                </div>
                <div><br>
                    <label for="state">Choose your State :</label>
                    <select name="state" id="state">
                        <option value="">Select State</option>
                        <?php 
                        if (isset($viewdata) && $viewdata != '') { ?>
                            <option value="<?php echo ($viewdata['state_id']); ?>"<?php echo 'selected="selected"'?>><?php echo ($viewdata['state_name']); ?></option>
                        <?php } ?>
                    </select>
                    <span class="text-danger"><?php echo form_error('state'); ?></span>
                </div>
                <div><br>
                    <label for="city">Choose your City :</label>
                    <select name="city" id="city">
                        <option value="">Select City</option>
                        <?php 
                        if (isset($viewdata) && $viewdata != '') { ?>
                            <option value="<?php echo ($viewdata['city_id']); ?>"<?php echo 'selected="selected"'?>><?php echo ($viewdata['city_name']); ?></option>
                        <?php } ?>
                    </select>
                    <span class="text-danger"><?php echo form_error('city'); ?></span>
                </div>
                <div><br>
                    <label for="address">Address :</label><br>
                    <textarea name="address" id="address" cols="25" rows="2" placeholder="Enter your address"><?php echo isset($viewdata['address'])?$viewdata['address']:''; ?></textarea>
                    <span class="text-danger"><?php echo form_error('address'); ?></span>
                </div>
                <div>
                    <label for="file_name">Select profile image:</label>
                    <input type="file" id="file_name" name="file_name" onchange="preview()">
                    <span><?php echo form_error('file','<p class="text-danger">','</p>') ?></span>
                    <input type="hidden" name="file_name" value="<?php echo isset($viewdata['file_name'])?$viewdata['file_name']:''; ?>">
                    <br>
                    <img  id="thumb" src="<?php echo base_url('imagesfolder/'.(isset($viewdata['file_name'])?$viewdata['file_name']:'')); ?>" alt="" width="100px" height="100px" />
                </div>
                <input type="hidden" name="id" value="<?php echo isset($viewdata['id'])?$viewdata['id']:''; ?>">                                                                        
                <br>
                <div>
                    <input type="submit" value="Submit" name="postSubmit">
                </div>
            </form>
        </div>
    </div>
</body>

</html>

<script>
    function preview() {
            thumb.src=URL.createObjectURL(event.target.files[0]);
        }
    $(document).ready(function() {
        $('#country').change(function() {
            var country_id = $('#country').val();
            if (country_id != '') {
                $.ajax({
                    url: "<?php echo base_url(); ?>main_controller/fetch_state",
                    method: "POST",
                    data: {
                        country_id: country_id
                    },
                    success: function(data) {
                        $('#state').html(data);
                        $('#city').html('<option value="">Select City</option>');
                    }
                });
            } else {
                $('#state').html('<option value="">Select State</option>');
                $('#city').html('<option value="">Select City</option>');
            }
        });

        $('#state').change(function() {
            var state_id = $('#state').val();
            if (state_id != '') {
                $.ajax({
                    url: "<?php echo base_url(); ?>main_controller/fetch_city",
                    method: "POST",
                    data: {
                        state_id: state_id
                    },
                    success: function(data) {
                        $('#city').html(data);
                    }
                });
            } else {
                $('#city').html('<option value="">Select City</option>');
            }
        });
    });
</script>