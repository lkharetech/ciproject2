<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <title>View Single Data Page</title>
</head>

<body>
    <div class="container">
        <div>
            <!-- <br> -->
            <h3 style="text-align: center;">VIEW DATA</h3>
            <button onclick="window.location.href='<?php echo site_url('main_controller'); ?>'">HOME</button>
            <hr>
        </div>
        <?php if(!empty($viewdata)) { //echo "<pre>"; print_r($viewdata); die;?>
            <div>
                <label for="">Profile pic :</label>
                <img src="<?php echo base_url('imagesfolder/'.$viewdata['file_name'])?>" alt="" width="100px" height="100px"><br><br>
                <label for="">Name :</label>
                <span><?php echo $viewdata['name']; ?><span><br>
                <label for="">Mobile No. :</label>
                <span><?php echo $viewdata['mobile']; ?><span><br>  
                <label for="">Email id :</label>
                <span><?php echo $viewdata['email']; ?><span><br>  
                <label for="">Gender :</label>
                <span><?php echo $viewdata['gender']; ?><span><br>  
                <label for="">Country :</label>
                <span><?php echo $viewdata['country_name']; ?><span><br>   
                <label for="">State :</label>
                <span><?php echo $viewdata['state_name']; ?><span><br>
                <label for="">City :</label>
                <span><?php echo $viewdata['city_name']; ?><span><br> 
                <label for="">Address :</label>
                <span><?php echo $viewdata['address']; ?><span><br>
                <br>
                <div>
                <button onclick="window.location.href='<?php echo site_url('main_controller/delete/' . $viewdata['id']); ?>'" onclick="return confirm('Are you sure to delete?')">Delete</button>
                </div>    
            </div>
        <?php } else { ?>
            <h2>Someting went wrong, try again</h2>
        <?php } ?>    

    </div>

</body>

</html>